This project contains 3 modules, such as parser.py (parsing code), database.py (connection to the Postgresql database with psycopg2, I used https://www.elephantsql.com/,
and a function which is need to add the data to the database.) and the main.py module. When main.py module being started, the info from https://www.kijiji.ca/b-apartments-condos/city-of-toronto/c37l1700273
is parsed, processed and added to the database.
To add data to your database, you need to change the values on database.py module (dbname, user, host, password), and then run the main file.

