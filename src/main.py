from bs4 import BeautifulSoup
from parser import get_page_source, get_title, get_desc, get_image_link, get_city, get_date, get_price, get_bed_amount
from database import add_to_db


def main():
    counter = 50
    while True:
        source = get_page_source(counter)
        if not source:
            break
        print(counter)

        cards = BeautifulSoup(source, 'html.parser').find_all('div', {'class': 'search-item'})
        for card in cards:
            title = get_title(card)
            desc = get_desc(card)
            image_link = get_image_link(card)
            city = get_city(card)
            date = get_date(card)
            currency, price = get_price(card)
            beds = get_bed_amount(card)

            add_to_db(title,desc,image_link, city, date, currency, price, beds)
            print(title)

        counter += 1


if __name__ == '__main__':
    main()
