import datetime

import requests
from bs4 import BeautifulSoup
import re


def get_page_source(page):
    link = f'https://www.kijiji.ca/b-apartments-condos/city-of-toronto/page-{page}/c37l1700273'
    source = requests.get(link).text

    if page == 1:
        return source

    bs = BeautifulSoup(source,'html.parser')
    current_page =bs.find('h1', {'class': 'resultsHeading-1285903303'}).text
    # print(current_page)
    current_page = int(re.findall(r'[0-9]*$', current_page)[0])
    if page != current_page:
        return False
    else:
        return source


def get_title(bs_obj):
    return bs_obj.find('a', {'class': 'title'}).text.strip()


def get_desc(bs_obj):
    return bs_obj.find('div', {'class': 'description'}).text.strip()


def get_image_link(bs_obj):
    return bs_obj.find('div', {'class': 'image'}).find('img').get('data-src')


def get_city(bs_obj):
    return bs_obj.find('div', {'class': 'location'}).find('span').text.strip()


def get_date(bs_obj):
    date = bs_obj.find('div', {'class': 'location'}).find('span', {'class': 'date-posted'}).text
    if re.search(r'\d* \w+', date) is not None:
        return datetime.date.today().strftime('%d-%m-%Y')
    else:
        return date.replace('/', '-')


def get_price(bs_obj):
    price = bs_obj.find('div', {'class': 'price'}).text.strip()
    if re.match(r'^.\d*,*\d*\.\d{2}', price) is not None:
        try:
            return price[0], float(price[1:].replace(',', ''))
        except ValueError:
            return None, None
    else:
        return None, None


def get_bed_amount(bs_obj):
    result = bs_obj.find('span', {'class': 'bedrooms'}).text.strip()
    return result.removeprefix('Beds:').strip()
