from psycopg2 import connect

con = connect(
    dbname='ryziawdg',
    user='ryziawdg',
    host='balarama.db.elephantsql.com',
    password='1vvTMAHq84W8p4RMoq03WoFeA0pJBL9i'
)

cursor = con.cursor()
cursor.execute("""
    CREATE TABLE IF NOT EXISTS data(
        id SERIAL PRIMARY KEY,
        title VARCHAR(300),
        description VARCHAR,
        image VARCHAR(300),
        city VARCHAR(100),
        date VARCHAR(50),
        currency VARCHAR(1),
        price REAL,
        beds VARCHAR(30)
    );

""")


def add_to_db(title, desc, image_link, city, date, currency, price, beds):
    cursor.execute("""
        INSERT INTO data(title, description, image, city, date, currency, price, beds)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
    """, [title, desc, image_link, city, date, currency, price, beds])
    con.commit()
